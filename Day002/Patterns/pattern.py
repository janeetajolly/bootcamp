LINEWIDTH = 40
STAR, SPACE = '*', ' '
LF = '\n'
SHARP, USCORE, BS = '#', '_', '\b'
#START, REPEAT, LAST = STAR, STAR+STAR, ''
#START, REPEAT, LAST = STAR, SPACE+STAR, ''
START, REPEAT, LAST = SHARP, USCORE+USCORE, BS+SHARP

def pattern(size: int):
    return [line(i) for i in range(size)]

def line(line_num: int) -> str:
    return start(line_num) + repeat(line_num) + last(line_num)

def start(n: int) -> str:
    return START

def repeat(n: int) -> str:
    return n * REPEAT 

def last(n: int) -> str:
    return LAST

def make_pyramid(size: int) -> str:
    return LF.join([line.center(LINEWIDTH) for line in pattern(size)])

def make_right(size: int) -> str:
    return LF.join([line.rjust(LINEWIDTH) for line in pattern(size)])

def make_arrow(size: int) -> str:
    base = pattern(size)
    arrow = base + base[::-1][1:]
    return LF.join([line.rjust(LINEWIDTH) for line in arrow])

def make_diamond(size: int) -> str:
    base = pattern(size)
    arrow = base + base[::-1][1:]
    return LF.join([line.center(LINEWIDTH) for line in arrow])

def make_hourglass(size: int) -> str:
    base = pattern(size)
    hourglass = base[::-1] + base[1:]
    return LF.join([line.center(LINEWIDTH) for line in hourglass])

print(make_pyramid(5))
#print(make_right(5))
#print(make_arrow(5))
#print(make_diamond(5))