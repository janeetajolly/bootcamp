def pattern2(n: int, alignment : int, width: int) -> str:
    star = 1
    pattern = []
    while n > 0:
        pattern.append(' ' * alignment + '* ' * star)
        n -= 1
        star += width
        alignment -= 2
    return '\n'.join(pattern)

print(pattern2(5,40,1))