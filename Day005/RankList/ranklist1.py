def format(student_list) -> list[list[str]]:
    data = list()
    with open(student_list) as f:
        for line in f:
            *name_list, mark = line.split()
            name = ' '.join(name_list)
            data.append([name, int(mark)])
    return data

def ranker(data: list[str]) -> list:
    return sorted(format(data), key = lambda student: student[1], reverse = True)

def rank_list(data: list[str]) -> list[str]:
    temp = 1
    data[0].insert(0, 1)
    previous = data[0][2]
    for rank, i in enumerate(data[1:], start = 2):
        if i[1] != previous:
            i.insert(0, rank)
            temp = rank
            previous = i[2]
        else:
            i.insert(0, temp)
    return data
       
formatted_data = ranker('marks.txt')
for i in rank_list(formatted_data):
    print(i)