#NEW APPROACH

def generate_primes(limit: int) -> list[int]:
    primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
    def is_prime(n: int) -> bool:
        pos = 3
        factor = primes[pos]
        while factor * factor <= n:
            if n % factor == 0:
                return False
            pos += 1
        return True
    
    for n in range(30, limit, 30):
        for step in [1, 7, 11, 13, 17, 19, 23, 29]:
            if is_prime(n + step):
                primes.append(n + step)
    return sorted(primes)

print(generate_primes(100))
