import pprint
import sys


LF, BLACK, WHITE = '\n', '#', ' '

# ---------------
# Load the grid
# ---------------

grid = ['WWWWWWBWWWWWWWW',
'WBWBWBWBWBWBWBW'
'WWWWWWWWWBWWWWW',
'WBWBWBWBWBWBWBW',
'WWWWWBWWWWWWWWW',
'WBWBBBWBWBWBWBW',
'WWWWWWWBWWWWBBB',
'WBWBWBBBBBWBWBW',
'BBBWWWWBWWWWWWW',
'WBWBWBWBWBBBWBW',
'WWWWWWWWWBWWWWW',
'WBWBWBWBWBWBWBW',
'WWWWWBWWWWWWWWW',
'WBWBWBWBWBWBWBW',
'WWWWWWWWBWWWWWW']
# -------------
# Add sentinel
# -------------
size = len(grid)
grid.insert(0, BLACK * size)
grid.append(BLACK * size)

grid = [BLACK + line + BLACK for line in grid]

# --------------
# Transpose grid
# --------------
transpose = []
for col in range(len(grid)):
     s = ''
     for row in grid:
        s += row[col]
     transpose.append(s)

# ------------------
# Combine into lines
# ------------------
across = ''.join(grid)
print(across)