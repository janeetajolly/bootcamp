def generate(data: tuple[int]) -> int:
    p = data[0]
    b = bpow =data[1]
    val = []
    for i in range(1, p):
        val.append(bpow)
        bpow = (b * bpow) % p
    return val

def total(data: list[int]) -> int:
    return len(set(data))

print(total(generate((101, 5))))
