#PRE PROCESSING DATA

SEP = ','

def is_ascending(reading: int) -> bool:
    if reading < 10:
        return True
    if reading % 10 <= (reading // 10) % 10:
        return False
    return is_ascending(reading //10)

def all_readings(size: int) -> list[int]:
    return [val for val in range(10 ** (size - 1), 10 ** size) if is_ascending(val)]

def to_repr(readings: list[int]) -> str:
    return SEP.join([str(reading) for reading in readings]) + '\n'

def from_repr(rep: str) -> list[str]:
    return [int(item) for item in rep.split(SEP)]

def save_readings(filename: int) -> None:
    with open(filename, 'w') as f:
        for size in range(1, 10):
            f.write(SEP.join([str(reading) for reading in all_readings(size)]))
            f.write('\n')

def load_readings(filename: int) -> dict[int, list[int]]:
    return {i + 1: from_repr(line) for i, line in enumerate(open(filename))}

def next_reading(reading: int) -> int:
    size = len(str(reading))
    readings = load_readings('readings.txt')[size]
    new_index = (readings.index(reading) + 1) % len(readings)
    return readings[new_index]

save_readings('readings.txt')

print(load_readings('readings.txt')[8])

print(next_reading(13456789))