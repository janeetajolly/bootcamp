def is_ascending(n: int) -> bool:
    if n < 10:
        return True
    elif (n // 10) % 10 >= n % 10:
        return False
    else:
        return is_ascending(n // 10)
    
def limits(n: int) -> tuple[int, int]:
    DIGITS = '123456789'
    size = len(str(n))
    return int(DIGITS[:size]), int(DIGITS[-size:])

def next_reading(reading: int) -> int:
    start, limit = limits(reading)
    if reading == limit:
        return start
    else:
        reading += 1
        while not is_ascending(reading):
            reading += 1
        return reading
    
def prev_reading(reading: int) -> int:
    start, limit = limits(reading)
    if reading == start:
        return limit
    else:
        reading -= 1
        while not is_ascending(reading):
            reading -= 1
        return reading

def forward(reading: int, steps: int = 1) -> int:
    start, limit = limits(reading)
    for _ in range(steps):
        if reading == limit:
            reading = start
        else:
            reading += 1
    while not is_ascending(reading):
        reading += 1
    return reading

def backward(reading: int, steps: int = 1) -> int:
    start, limit = limits(reading)
    for _ in range(steps):
        if reading == start:
            reading = limit
        else:
            reading -= 1
    while not is_ascending(reading):
        reading -= 1
    return reading

def disance(a_reading: int, b_reading: int) -> int:
    count = 0
    while a_reading != b_reading:
        a_reading = next_reading(a_reading)
        count += 1
    return count