def ascending(s: int):
    return str(s) == ''.join(sorted(set(s)))

def possible_readings(size: int):
    lower, higher = 10 ** (size-1), 9 * int('1' * size)
    return [s for s in range(lower, higher) if ascending(s)]

def next_reading(reading: int, readings: list):
    if readings.index(reading) == len(readings) - 1:
        return readings[0]
    return readings[readings.index(reading) + 1]

def prev_reading(reading: int, readings: list):
    if readings.index(reading) == 0:
        return readings[-1]
    return readings[readings.index(reading) - 1]

def reading_after(reading: int, steps: int, readings: list):
    if readings.index(reading) > len(readings) - 1 - steps:
        return readings[steps - (len(readings) - readings.index(reading))]
    return readings[readings.index(reading) + steps]

def reading_before(reading: int, steps: int, readings: list):
    if readings.index(reading) < steps:
        return readings[-1 * (steps - readings.index(reading))]
    return readings[readings.index(reading) - steps]

def distance(a_reading: int, b_reading: int, readings: list):
    return abs(readings.index(a_reading) - readings.index(b_reading))