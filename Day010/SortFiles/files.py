def get_filename(files: list[str]) -> list[str]:
    filename = []
    for i in files:
        filename.append(''.join([j for j in i if j.isalpha()]))
    return filename

def get_num(files: list[str]) -> list[str]:
    dates = []
    for i in files:
        dates.append(''.join([j for j in i if j.isdigit()]))
    return dates

def make_dict(data: list[str]) -> dict[str, str]:
    dates = get_num(data)
    filename = get_filename(data)
    files = dict(zip(dates, filename))
    return files

def sort_files(files: dict[str,str]) -> list[str]:
    new_files = []
    order = sorted(files.keys(), key=int)
    for i in order:
        new_files.append(files[i] + i)
    return new_files

print(sort_files(make_dict(['Day1', 'Day11', 'Day2', 'Day10'])))