HYPHEN = '-'
def clean(isbn: str) -> list[int]:
    return [int(ch) for ch in isbn if ch!= HYPHEN]

def check_digit(isbn: str) -> int:
    weights = [1,3] * 6
    check_digit = sum([a * b for (a, b) in zip(weights, clean(isbn))]) % 10
    return 0 if check_digit == 0 else 10 - check_digit

def check_isbn(isbn: str) -> int:
    return check_digit(isbn[:-1]) == int(isbn[-1])

print(check_isbn('978-0-596-52068-7'))