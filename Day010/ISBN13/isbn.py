def clean(code: str) -> str:
    return ''.join(code.split('-'))

def find_sum(code: str) -> int:
    total = 0
    for index, d in enumerate([int(i) for i in clean(code)], start = 1):
        if index % 2 == 1:
            total += d
        else:
            total += 3 * d 
    return total

def get_digit(number: int) -> int:
    if number == 0:
        return 0
    else:
        return 10 - number % 10

def add_digit(code: str) -> str:
    return code + str(get_digit(find_sum(code)))

print(add_digit('978-0-596-52068-'))