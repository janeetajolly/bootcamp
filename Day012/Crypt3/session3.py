#CODE IN SAGE MATH
#--------------------
#Suppose we know 7^1000 = 7
#e * d = 1000
#7^1000 = (7^e)^d = 7^(e*d) = 7^(1000) = 7
#ed is chosen such that x^(ed) = x
'''
p = next_prime(1200)
q = next_prime(p + 1)
m = N * (p - 1) * (q - 1) + 1     #N is free
(x^(p - 1)(q - 1)) ^N = 1
print("n =", p * q)
print(m, '=', factor(m))
'''

#n = 1456813
#1454401 = 13 * 17 * 6581

#Euler's theorem guarantees x^m = x for 'most' x
'''(x^1454401) % n = x for almost all the times '''
#n a product of two huge primes p and q
#To encrypt a general x we need to know e and n PUBLIC KEY
#To decrpyt we need to know d and the same n PRIVATE KEY
#from e to calculate d
#one must know the individual factors p and q of n
#e is such that gcd(e, (p-1)(q-1)) = 1
#d is obtained by Extdd Euc alg for (p-1)(q-1) and e pair
#the inverse there will be the d; (pq - p - q + a)

#SETTING UP RSA
'''
Pick two huge primes p and q
n = p * q
pick e, some number such that gcd of e with (p-1)*(q-1) = 1

Make e and n public knowledge

to encrypt x simply calculate: y = x^e % n

-----------------------------------------

DECRYPTIO KEY CALC
modular inverse of e wrt (p-1)*(q-1)
use extended euclid algo

Decryption of y is: y^d % n

Euclid's theorem (from 1750's) guarantees that:
    y^d = x^(ed) = x^1 = x % n
So one can reconver original message

------------------------------------------
'''

#Typically e is chosen small number. e should not have any common factor with p-1 or q-1.
#(If you choose e to be prime just check e does not divide (p-1) or (q-1))

'''
p = 17
q = 13
n = 221
(p-1)(q-1) = 16*12 = 192
e can be any prime other than 2 and 3
e = 35
Public Key is e = 35, n = 221

d is modular inverse of 35 % 192
        q   r   x   y   
           192  1   0
        5  35   0   1
        2  17   1  -5
           1   -2   11

d = 11
y = x^35 % 221
x = y^11 % 221
Encryption is possible only for x not a multiple of p and q.

How to calculate x^e mod n

Calculate binary of e, eg binary of e = 10110001
        1   0   1   1   0   0   0   1
       128 64  32   16  8   4   2   1        (from the right) -> 177

    x^e = x^177 = x^1 * x^16 * x^32 * x^128
                  -------------------------
                These individual terms are obtainable by iterated squaring
'''
