class Odometer:
    pass

o1 = Odometer()
o2 = Odometer
print(o1 == o2)                 #now 2 odometers have different identities

#--------------
o1prev = 1234
o2prev = 1234         #previously, 2 odometers having same reading would be considered same
#--------------

#-------------------------------------
class LightBulb:
    pass
l1 = LightBulb()     #think lightbulb has 2 states -> ON and OFF
l1.shining = False
print(l1.shining)
l2 = LightBulb()
l2.shining = True
print(l1.shining, l2.shining)  #2 states

#--------------------------------------

def sqrt(num: int) -> float:
    return -1 if num < 0 else num ** 0.5

#line 26 is the sintacting sugar of
'''
    if num < 0:
        return -1
    else:
        return num ** 0.5
'''