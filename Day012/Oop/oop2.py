DIGITS = '123456789'           #class variable

def min_max(size: int) -> tuple[int, int]:
    return int(DIGITS[:size]), int(DIGITS[-size:])

class Odometer:                                  #class is type
    def __init__(self, size: int) -> None:       #initializer, self -> syntacting sugar
        self.reading = min_max(size)[0]          # init does not return anything

    def __str__(self):
        return f'<{self.reading}>'
    
    def __repr__(self):
        return f'Size: {self.size}, Reading: {self.reading}'

o1= Odometer(3)                                   #o1 and o2 are objects
o2 = Odometer(4)
print(o1 == o2)                         
print(o1.reading)                           #reading and init are properties
print(o2.reading)                           #o1.reading and o2.reading are states
print(o1)                                     

#printing is meant for end users and inspecting is meant for programmers
#with __str__ we are able to print(o1)
#in python tutor we can inspect a variable

# __ -> magic methods, dunder methods
# __str__ -> provides functionality of printing
# __repr__ -> provides functionality for inspecting an object
# python uses repr if str not present 
#   but it will not use str if repr is missing
# __hash__ -> sets
# __lt__ -> sorted
'''
             HAVE __str__                    DONT HAVE __str__
HAVE         inspect: uses __repr__           inspect: uses __repr__
__repr__     print: uses __str__              print: uses __repr__

DONT HAVE    inspect: prints address          inspect: address
__repr__     print: uses __str__              print: address
'''
