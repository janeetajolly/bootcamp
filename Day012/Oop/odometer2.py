DIGITS = '123456789'           

def min_max(size: int) -> tuple[int, int]:
    return int(DIGITS[:size]), int(DIGITS[-size:])

class Odometer:
    def __init__(self, size: int) -> None:
        self._size = size
        self._min, self._max = min_max(size)
        self.reading = min_max(size)[0]

    def __str__(self):
        return f'<{self.reading}>'
    
    def __repr__(self):
        return f'Size: {self.size}, Reading: {self.reading}'
    
    @classmethod
    def is_ascending(cls, reading) -> bool:
        return all(a < b for a, b in zip(str(reading), str(reading)[1:]))

    def forward(self, step: int=1) ->  int:
        copy = Odometer(self._size)
        copy.reading = self.reading
        for _ in range(step):
            if copy.reading == self._max:
                copy.reading = self._min
            else:
                copy.reading += 1
            while not Odometer.is_ascending(copy.reading):
                copy.reading += 1
        return copy

    def backward(self, step: int=1) -> int:
        copy = Odometer(self._size)
        copy.reading = self.reading
        for _ in range(step):
            if copy.reading == self._min:
                copy.reading = self._max
            else:
                copy.reading -= 1
            while not Odometer.is_ascending(copy.reading):
                copy.reading -= 1
        return copy

    def distance(self, other) -> int:
        count = 0
        copy = Odometer(self._size)
        copy.reading = self.reading
        while copy.reading != other.reading:
            count += 1
            Odometer.forward(copy)
        return count



o1= Odometer(3)
o1 = Odometer.forward(o1,5)

o2 = Odometer(3)

o2 = Odometer.backward(o2,1)


