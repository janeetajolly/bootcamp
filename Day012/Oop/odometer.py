
DIGITS = '123456789'           

def min_max(size: int) -> tuple[int, int]:
    return int(DIGITS[:size]), int(DIGITS[-size:])

class Odometer:
    def __init__(self, size: int) -> None:
        self._size = size
        self._min, self._max = min_max(size)
        self.reading = min_max(size)[0]

    def __str__(self):
        return f'<{self.reading}>'
    
    def __repr__(self):
        return f'Size: {self.size}, Reading: {self.reading}'
    
    @classmethod
    def is_ascending(cls, reading) -> bool:
        return all(a < b for a, b in zip(str(reading), str(reading)[1:]))

    def forward(self, step: int=1) ->  None:
        for _ in range(step):
            if self.reading == self._max:
                self.reading = self._min
            else:
                self.reading += 1
            while not Odometer.is_ascending(self.reading):
                self.reading += 1

    def backward(self, step: int=1) -> None:
        for _ in range(step):
            if self.reading == self._min:
                self.reading = self._max
            else:
                self.reading -= 1
            while not Odometer.is_ascending(self.reading):
                self.reading -= 1

    def distance(self, other) -> int:
        count = 0
        temp = self.reading
        while self.reading != other.reading:
            count += 1
            Odometer.forward(self)
        self.reading = temp
        return count



o1= Odometer(3)
o2 = Odometer(3)
print(o1)
Odometer.forward(o1,5)
Odometer.backward(o2,1)
print(Odometer.distance(o1, o2))
print(o1)

