DIGITS = '123456789'           #class variable

def min_max(size: int) -> tuple[int, int]:
    return int(DIGITS[:size]), int(DIGITS[-size:])

class Odometer:                                  #class is type
    def __init__(self, size: int) -> None:       #initializer, self -> syntacting sugar
        self._size = size
        self._min, self._max = min_max(size)
        self.reading = min_max(size)[0]          # init does not return anything

    def __str__(self):
        return f'<{self.reading}>'
    
    def __repr__(self):
        return f'Size: {self.size}, Reading: {self.reading}'
    
    def __eq__(self, other) -> bool:             #not necessary
        if self._size != other._size:
            raise ValueError(f'Incomparable Odometer: sizes {self._size}, {other._size}')
        return self.reading == other.reading
    
    @classmethod
    def is_ascending(cls, reading) -> bool:
        return all(a < b for a, b in zip(str(reading), str(reading)[1:]))

    def forward(self, step: int=0) ->  None:
        for _ in range(step):
            if self.reading == self._max:
                self.reading = self._min
        else:
            self.reading += 1
            while not Odometer.is_ascending(self.reading):
                self.reading += 1

o1= Odometer(3)                                   #o1 and o2 are objects
o2 = Odometer(4)
print(o1)
o1.reading
print(o1)
print(Odometer.is_ascending(o1.reading))
Odometer.forward(o1)
o1.forward()           #35 is syntacting sugar of 34
print(o1)

o5 = Odometer(5)
o6 = Odometer(5)

print(o5 == o6)
print(o5.__eq__(o6))
print(o5 is o6)        #shallow equality

o6.reading = -233    #   -> we can set values
print(o6)

# putting _ in front of all readings (_reading) tells that u are not supposed
#   to directly change the value of the odometers (line 48)