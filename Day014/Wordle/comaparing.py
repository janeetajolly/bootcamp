'''
def compare_words(guess: str, correct: str) -> str:
    compared = []
    for i in range(len(correct)):
        if guess[i] == correct[i]:
            compared.append( guess[i] + " ")
        elif guess[i] in correct:
            compared.append("'" + guess[i] + "' ")
        else:
            compared.append("#" + guess[i] + "# ")
    return ''.join(compared)


print(compare_words('TEARS', 'EXALT'))

'''
'''

def compare_words(guess: str, correct: str) -> str:
    compared = []
    for i in range(len(correct)):
        if guess[i] == correct[i]:
            compared.append(guess[i] + " ")
        elif guess[i] in correct:
            if guess.count(guess[i]) == correct.count(guess[i]):
                compared.append("'" + guess[i] + "' ")
            else:
                if i == guess.index(guess[i]):
                    compared.append("'" + guess[i] + "' ")
                else:
                    compared.append("#" + guess[i] + "# ")
        else:
            compared.append("#" + guess[i] + "# ")
    return ''.join(compared)


print(compare_words('TEARE', 'EXALT'))

'''

def compare_words(guess: str, correct: str) -> str:
    compare = list(zip(correct, guess))
    compared = []
    for i in compare:
        if i[0] == i[1]:
            compared.append(i[0])
        elif i in compare[0]:
            compared.append('' )

print(compare_words("TEARE", "EXALT"))
