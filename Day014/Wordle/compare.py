RED = 'r'
YELLOW  = 'y'
GREEN = 'g'
def judge(guess: str, answer: str):
    feedback = ''
    guess, answer = guess.lower(), answer.lower()
    for g_ch, a_ch in zip(guess, answer):
        if g_ch not in answer:
            feedback += RED
        elif g_ch == a_ch:
            feedback += GREEN
        else:
            feedback += YELLOW
    return feedback

print(judge('SPEAR', 'EXALT'))

