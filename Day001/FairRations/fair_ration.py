def even(k: int) -> bool:
    return k % 2 == 0

def verify(queue: list[int]) -> str:
    if not even(sum(queue)):
        return "NO"
    else:
        return str(distribute(queue))
    
def distribute(queue: list[int]) -> int:
    if len(queue) == 1:
        return 0
    elif even(queue[0]):
        return distribute(queue[1:])
    else:
        queue = [queue[1] + 1] + queue[2:]
        return 2 + distribute(queue)
    
print(verify([2, 3, 4, 5, 6]))