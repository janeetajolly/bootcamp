def clean(equation: str) -> str:
    return ''.join([ch for ch in equation if ch != ' '])
    
def terms(equation: str) -> list[str]:
    return equation.replace('-','+-').split('+')

def reverse(term: list[str]) -> list[str]:
    return terms(term)[::-1]

def convert(term: list[int]) -> str:
    return '+'.join(reverse(clean(term))).replace('+-','-')

print(convert('7x^5+6x^4-3x^2+7'))