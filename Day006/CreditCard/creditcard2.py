def digit_sum(n: int) -> int:
    return 1 + (n - 1) % 9

def new_digits(n: int) -> list[int]:
    def new_digit(i: int, d: str) -> int:
        if i % 2 == 0:
            return digit_sum(int(d + d))
        else:
            return int(d)
    
    return [new_digit(*_) for _ in enumerate(reversed(str(n)))][::-1]

def check_digit(n: int) -> int:
    return 10 - (sum(new_digits(n)) % 10)
    
def validate(n: int) -> bool:
    return n % 10 == check_digit(n // 10)

print(validate(17893729974))