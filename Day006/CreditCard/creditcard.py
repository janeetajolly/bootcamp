# start from the rightmost digit
# moving left, double the value of every second digit (including the rightmost digit)
# sum the values of the resulting digits, say N
# check digit is the smallest number (possibly zero) that must be added to s
# to make a multiple of 10


def digit_sum(n: int) -> int:
    return 1 + (n - 1) % 9

def find_sum(n: int) -> int:
    total = 0
    for index, d in enumerate([int(i) for i in str(n)[::-1]]):
        if index % 2 == 0:
            total += digit_sum(d * 2)
        else:
            total += d
    return total
        
def check_digit(n: int) -> int:
    return 10 - find_sum(n) % 10

print(check_digit(1789372997))