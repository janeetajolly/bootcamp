def mod_inverse(m : int , n : int) -> int:
    if m > n:       
        r1, r2 = m, n
    else:
        r1, r2 = n, m
    x1 = y2 = 1
    x2 = y1 = 0

    while r2 != 1:
        q = r1 // r2
        r3 = r2
        r2 = r1 - q * r2
        r1 = r3
        x1, x2 = x2, x1 - q * x2
        y1, y2 = y2, y1 - q * y2
    return y2 

print(mod_inverse(23, 4036032))