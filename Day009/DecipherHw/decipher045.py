from collections import Counter
data = open("mono_ciph045.txt").read()
freq = Counter(data)
freq.most_common(5)

word_freq = Counter(data.split())
word_freq.most_common(5)

data1 = data.replace('t', 'T').replace(',', 'H').replace('5', 'E').replace('p', 'O')

data2 = data1.replace('b', 'M').replace('u', 'A').replace('4', 'I').replace('?', 'L').replace(".", 'J')

data3 = data2.replace('o', 'G').replace('l', 'R').replace('h', 'S').replace('m', 'N').replace('!', 'D').replace('&', 'F').replace('v', 'U')

data4 = data3.replace('a', 'C').replace('x', '.').replace('d', 'V').replace('8', 'P').replace('c', 'B').replace('g', 'W').replace('w', 'K')

data5 = data4.replace('1', 'Y').replace('-', '"').replace('r', 'X').replace('@', ',').replace('y', '-').replace('3', '?').replace('z', ';')

print(data5)