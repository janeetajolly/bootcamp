from collections import Counter
data = open("mono_ciph005.txt").read()
freq = Counter(data)
freq.most_common(5)

word_freq = Counter(data.split())
word_freq.most_common(5)

data1 = data.replace("i", "T").replace("r", "H").replace("v", "E")

data2 = data1.replace("z", "O")

data3 = data2.replace("p", "Y").replace("t", "B")

data4 = data3.replace('c', 'A').replace('d', 'L')

data5 = data4.replace('@', 'W').replace('h', 'S').replace('2', 'I').replace('f', 'C')

data6 = data5.replace('9', 'F').replace("'", 'N').replace('y', 'D').replace('e', 'U')

data7 = data6.replace('-', 'G').replace('!', 'M').replace('4', 'V')

data8 = data7.replace('l', 'R').replace('o', 'P').replace('s', '.').replace('k', 'Q').replace('?', ',').replace('*', 'K').replace('u','"').replace('b','?')

data9 = data8.replace('0', 'X').replace('8', '-').replace('1', 'J').replace('&', '!')

print(data9)