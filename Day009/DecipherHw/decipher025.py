from collections import Counter
data = open("mono_ciph025.txt").read()
freq = Counter(data)
freq.most_common(5)

word_freq = Counter(data.split())
word_freq.most_common(5)

data1 = data.replace('x', 'T').replace('h', 'H').replace('8', 'E').replace('d', 'O').replace('&', 'I')

data2 = data1.replace('u', 'N').replace('0', 'A').replace('9', 'W').replace('e', 'S').replace('f', 'Y').replace(';', 'D')

data3 = data2.replace('o', 'R').replace('m', 'B').replace('.', 'G').replace('i', 'U').replace('s', 'M')

data4 = data3.replace('?', 'F').replace('n', 'L').replace('k', 'P').replace('5', 'C').replace('z', 'K').replace('p', 'V')

data5 = data4.replace('v', '-').replace('b', 'J').replace('a', 'X').replace('@', '.').replace('r', ',')

data6 = data5.replace('4', 'Z').replace('l', '?').replace('1', '"')

print(data6)
