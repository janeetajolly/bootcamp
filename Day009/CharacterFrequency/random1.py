import random
import string

def genstr(n: int) -> str:
    chances = string.ascii_uppercase
    randomstr = [random.choice(chances) for i in range(5)]
    return ''.join(randomstr)

weight = {'A': 8.04, 'B': 1.48, 'C': 3.34, 'D': 3.82, 'E': 12.49, 'F': 2.40, 'G': 1.87,
            'H': 5.05, 'I': 7.57, 'J': 0.16, 'K': 0.54, 'L': 4.07, 'M': 2.51, 'N': 7.23,
            'O': 7.64, 'P': 2.14, 'Q': 0.12, 'R': 6.28, 'S': 6.51, 'T': 9.28, 'U': 2.73,
            'V': 1.05, 'W': 1.68, 'X': 0.23, 'Y': 1.66, 'Z': 0.09}

def weighted_gen(n: int) -> str:
    chances = string.ascii_uppercase
    randomstr = random.choices(chances, weights=weight.values(), k=n)
    return ''.join(randomstr)

def str_statistics(sample: str) -> dict[str, int]:
    stat = {key : 0 for key in string.ascii_uppercase} 
    for i in stat:
        stat[i] = sample.count(i) / len(sample) * 100
    return stat
    
word = weighted_gen(50)
print(word)
freq = str_statistics(word)
for i in freq:
    print(f"{i} {freq[i]:5} {weight[i]:5}")